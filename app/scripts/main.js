/* jshint devel:true */
'use strict';

$(document).ready(function () {
  var playlistID,
      containerId,
      maxResults,
      featuredId,
      targetId,
      player;

  playlistID  = 'PL5x_ypm62QeuvX5uvW0znHpkSZlFVIt9A';
  featuredId  = 'featured-one';
  containerId = 'section-one';
  maxResults  = 16;

  /*$('.section').on('click', function (e) {
    e.stopPropagation();
    $("body").animate({ scrollTop: '-' + $('body').height()}, 1000);
    console.log(event.pageY);
  });*/

  $('.menu-links a').on('click', function (e) {

    e.preventDefault();
    $('iframe').remove();
    $('.menu-links a, .section').removeClass('active');
    $(this).addClass('active');
    targetId    = $(this).attr('target-id');
    containerId = targetId;
    maxResults  = $(this).attr('results');
    playlistID  = $(this).attr('playlist-id');
    featuredId  = $(this).attr('featured-id');

    $('.section#' + targetId).addClass('active');

    if ($('#' + containerId + ' .list-videos .video-thumb').length > 0) {
      $('#' + containerId + ' .list-videos').masonry().masonry('reloadItems');
    } else {
      return getItems();
    }
  });

  var getItems = function () {
    $.ajax({
      url: 'https://www.googleapis.com/youtube/v3/playlistItems',
      type: 'GET',
      dataType: 'json',
      data: {
        part: 'snippet',
        playlistId: playlistID,
        key: 'AIzaSyDQBWjaD5cAhS8n-7OJgLUenxobnEXHasg',
        maxResults: maxResults
      },
      success: function (response) {
        var items,
            item,
            featuredVideo,
            featuredBg,
            videoContainer,
            videoContainerId,
            transitionProp,
            transitionEndEvent;

        items         = response.items;
        featuredVideo = items[0].snippet.resourceId.videoId;
        featuredBg    = items[0].snippet.thumbnails.high.url;

        $('#' + featuredId + ' .video-large').css({
          'background': 'url(' + featuredBg + ') no-repeat center center',
          'display'   : 'block'
        }).animate({
          'opacity': 1
        });

        $('#' + featuredId + ' .video-large-inner').attr('video-id', featuredVideo);
        $('#' + featuredId + ' .featured-title-inner').text(items[0].snippet.title).addClass('title');
        $('#' + featuredId + ' .fb-share').attr('href', 'http://www.facebook.com/share.php?u=https://www.youtube.com/watch?v='+ items[0].snippet.resourceId.videoId).attr('video-title',  items[0].snippet.title);
        $('#' + featuredId + ' .tw-share').attr('href', 'https://twitter.com/share?url=https://www.youtube.com/watch?v='+ items[0].snippet.resourceId.videoId).attr('video-title',  '"' + items[0].snippet.title + '"');

        item = '';

        for (var i in items) {
          if (i > 0) {
            item += '<div class="video-thumb" video-id="' + items[i].snippet.resourceId.videoId + '">' +
                    '<div class="video-thumb-inner" style="background: url(' + items[i].snippet.thumbnails.medium.url + ') no-repeat center center">' +
                    '<span class="button-player"></span>' +
                    '</div>'  +
                    '<span class="video-title">' +
                    '<span class="video-title-inner pull-left">' +
                    '<span class="excerpt">' + items[i].snippet.title.substr(0, 20) + ' ' + (items[i].snippet.title.length >= 20 ? '...' : '') + '</span>' +
                    '<span class="full">' + items[i].snippet.title + '</span></span>' +
                    '<span class="pull-right share-icons">' +
                    '<a class="fb-share" href="http://www.facebook.com/share.php?u=https://www.youtube.com/watch?v='+ items[i].snippet.resourceId.videoId + '" video-title="' + items[i].snippet.title + '">' + '</a>' +
                    '<a class="tw-share twitter-share-button" href="https://twitter.com/share?url=https://www.youtube.com/watch?v='+ items[i].snippet.resourceId.videoId + '" video-title="' + items[i].snippet.title + '">' + '</a>' +
                    '</span>' +
                    '</span>' +
                    '</div>';
          }
        }

        transitionProp = getStyleProperty('transition');
        transitionEndEvent = {
          WebkitTransition: 'webkitTransitionEnd',
          MozTransition   : 'transitionend',
          OTransition     : 'otransitionend',
          transition      : 'transitionend'
        }[transitionProp];

        $('#' + containerId + ' .list-videos').html(item);

        setTimeout(function () {
          $('#' + containerId + ' .list-videos').masonry({
            itemSelector: '.video-thumb',
            columnWidth : 245,
            gutter      : 5
          });
        },100);

        $('#' + containerId + ' .video-large-inner' +',' + '#' + containerId + ' .video-thumb-inner').on('click', function (e) {
          e.stopPropagation();

          if ($(this).parent().hasClass('is-expanded')) {
            return;
          }

          $('.video-large').removeClass('large-hoverled');

          if ($(this).is('.video-large-inner')) {
            $(this).parent().addClass('large-hoverled');
          }

          $('.video-title, .featured-title').css({'display': 'block'});

          $(this).next('.video-title, .featured-title').css({'display': 'none'});

          $('iframe').remove();

          videoContainer   = $(this);
          videoContainerId = videoContainer.attr('video-id');

          if ($('.video-thumb').hasClass('is-expanded')) {
            $('.video-thumb').removeClass('is-expanded hoverled');
            $('#' + containerId + ' .list-videos').masonry();
          }

          if ($(this).is('.video-thumb-inner')) {
            videoContainerId = videoContainer.parent().attr('video-id');

            var $this = $(this);
            var previousContentSize    = getSize( this );
            this.style[transitionProp] = 'none';
            $this.css({
              width: previousContentSize.width,
              height: previousContentSize.height
            });

            var $itemElem = $this.parent().addClass('is-expanded');

            var redraw = this.offsetWidth;

            this.style[transitionProp] = '';

            if (transitionProp) {
              var _this = this;
              var onTransitionEnd = function() {
                _this.style.width  = '';
                _this.style.height = '';
              };
              $this.one(transitionEndEvent, onTransitionEnd);
            }

            var size = getSize($itemElem[0]);

            $this.css({
              width : size.width,
              height: size.height
            });

            $('#' + containerId + ' .list-videos').masonry();
          }

          setTimeout(function () {
            videoContainer.append('<iframe src="https://www.youtube.com/embed/' + videoContainerId + '?autoplay=true&showinfo=false&controls=0&enablejsapi=1"' + ' frameborder="0" allowfullscreen>' + '</iframe>');
          }, 100);

          setTimeout(function () {
            $itemElem.addClass('hoverled');

            $('html, body').animate({
              scrollTop: $itemElem.offset().top
            }, 1000);
          }, 1000);
        });

        $('.fb-share, .tw-share').click(function(e) {
          e.preventDefault();
          e.stopPropagation();

            var left  = ($(window).width() / 2) - (555 / 2);
            var top   = ($(window).height() / 2) - (420 / 2);

            window.open($(this).attr('href'), 'popup', "width=555, height=420, top=" + top + ", left=" + left);
        });
      }
    });
  };
  return getItems();
});
